/*
 * Copyright (c) StreetHawk, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

#import "PointziReact.h"
#import "StreetHawkCore_Pointzi.h"

@implementation PointziReact

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(tagCuid:(NSString *)uniqueId)
{
  [StreetHawk tagCuid:uniqueId];
}

RCT_EXPORT_METHOD(tagUserLanguage:(NSString *)language)
{
  [StreetHawk tagUserLanguage:language];
}

RCT_EXPORT_METHOD(tagString:(NSString *)key value:(NSString *)value)
{
  [StreetHawk tagString:value forKey:key];
}

/**
 * Sends ViewName to Pointzi for Tips to target.
 * @param reactViewName name of View
 */
RCT_EXPORT_METHOD(viewWillRender:(NSString *)reactViewName)
{
    [StreetHawk setReactViewName:reactViewName];
}

RCT_EXPORT_METHOD(tagNumeric:(NSString *)key value:(double)value)
{
  [StreetHawk tagNumeric:value forKey:key];
}

RCT_EXPORT_METHOD(tagDatetime:(NSString *)key value:(NSString *)ISO8601DateString)
{
  NSDate *date = [RCTConvert NSDate:ISO8601DateString];
  [StreetHawk tagDatetime:date forKey:key];
}

RCT_EXPORT_METHOD(removeTag:(NSString *)key)
{
  [StreetHawk removeTag:key];
}

RCT_EXPORT_METHOD(incrementTag:(NSString *)key)
{
  [StreetHawk incrementTag:key];
}

RCT_EXPORT_METHOD(incrementTag:(NSString *)key value:(double)value)
{
  [StreetHawk incrementTag:value forKey:key];
}

@end
